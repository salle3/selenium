package mdas.selenium

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.openqa.selenium.*
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import java.util.concurrent.TimeUnit

// https://blog.kotlin-academy.com/your-first-selenium-data-scraper-in-kotlin-51ce22083fb9
class VuelingTest {

    private lateinit var driver: WebDriver

    @BeforeEach
    fun config() {
        //System.setProperty("webdriver.chrome.logfile", "D:\\chromedriver.log")
        System.setProperty("webdriver.chrome.verboseLogging", "true")
    }

    @Test
    fun searchVueling() {
        driver = ChromeDriver()
        driver.get("https://www.vueling.com/es")
        waitUntilPageIsReady(driver)

        driver.findElement(By.id("ensCloseBanner"))?.click()
        waitOneSecond(driver)

        // Caca, poc decidit fins ara!!
        val fullPane = driver.findElement(By.cssSelector(".pane-wrapper"))
        val inputs = fullPane.findElement(By.cssSelector(".form-group"))
        // TODO origin!!
        inputs.findElement(By.cssSelector("vy-airport-selector.form-input:nth-child(1)")).click()
        waitOneSecond(driver)
        driver.findElement(By.id("popup-list"))
            .findElements(By.className("liStation"))
            .first().click()
        waitOneSecond(driver)
        // TODO destino!! vy-airport-selector.form-input:nth-child(2)
        inputs.findElement(By.cssSelector("vy-airport-selector.form-input:nth-child(2)")).click()
        waitOneSecond(driver)
        driver.findElement(By.id("popup-list"))
            .findElements(By.className("liStation"))
            .first().click()
        waitOneSecond(driver)

        fullPane.findElement(By.id("btnSubmitHomeSearcher")).click()
        waitUntilPageIsReady(driver)

        if (driver.windowHandles.size != 1) driver.close()

        driver.quit()
    }

    /* #wrapper-fix
    .searchbar-wrap
    // On hi ha tot el que volem (boto mes 4 elements per omplir)
        CSS Selector: .pane-wrapper
        CSS Path    : html body.es-ES div#wrapper-fix div.searchbar-wrap.no-tabs flights-filter div#main-searchbar.searchbar-container div#searchbar.searchbar-main.container div.tab-content.searchbar-main_tab-content div#tab-search.tab-pane.searchbar-main_tab-content_pane.search.active div.pane-wrapper.ng-pristine.ng-invalid.ng-touched
        XPath       : /html/body/div[3]/div[2]/flights-filter/div/div/div/div/div
    */
    private fun curios(driver: ChromeDriver) {
        val webElement: WebElement? = driver.findElementByCssSelector(".pane-wrapper")
        val searchContext: SearchContext? = driver.findElementByCssSelector(".pane-wrapper")
        val takesScreenshot: TakesScreenshot? = driver.findElementByCssSelector(".pane-wrapper")
    }

    private fun waitUntilPageIsReady(driver: WebDriver) {
        val executor = driver as JavascriptExecutor
        WebDriverWait(driver, 1)
            .until { executor.executeScript("return document.readyState") == "complete" }
    }

    private fun waitOneSecond(driver: WebDriver) {
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS)
    }

    /* TODO screenshots
            val scrFile = (driver as TakesScreenshot).getScreenshotAs<File>(OutputType.FILE)
        copyFile(scrFile, File("./image.png"))

     */
}